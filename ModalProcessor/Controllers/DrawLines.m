function DrawLines(x, y, z)

pairs = [1 2 2 4 4 6 6 8 8 9 9 7 7 5 5 3 3 1];

for i = 1 : 9
    
	line([x(pairs(2*i-1)) x(pairs(2*i))], [y(pairs(2*i-1)) y(pairs(2*i))], [z(pairs(2*i-1)) z(pairs(2*i))], ...
                'Color', 'black', 'LineStyle', '-')
end