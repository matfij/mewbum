function PlotModel(handles, f, scale)

if (get(handles.dataType, 'Value') == 0)
    load('points')
else
    load('pointsAuto')
end

t = 0 : 0.05 : 8;

for j = 1 : length(t)

    for i = 1 : 9
        
        hold on
        view(300, 60)
        
        x(i) = points(i).cx;
        y(i) = points(i).cy;
        z(i) = sin(t(j)) * points(i).f(f);
        
        if (z(i) > 0)
            plot3(x(i), y(i), z(i), 'or')
        else
            plot3(x(i), y(i), z(i), 'ob')
        end
        
        xlim([-1 4])
        ylim([-1 4])
        zlim([-scale, scale]) 
        
    end
    
    DrawLines(x, y, z)
    
    pause(0.01)
    cla

end