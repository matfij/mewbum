function [a11, a12, a21, a22] = Analize(point, method)

warning off
load('points')

addpath ../Data

switch point
    case {1, 2, 3}
        load('rawx1x2x3')
    case {4, 5, 6}
        load('rawx4x5x6')
        point = point - 3;
    case {7, 8, 9}
        load('rawx7x8x9')
        point = point - 6;
end

Fs = 25000;
us = 1;%3176;
x1s = 8705;%11879;
x2s = 17409;%20583;
x3s = 26113;%29287;
len = 8700;

u = (x(us:us+len, 1) + x(us:us+len, 2) + x(us:us+len, 3) + x(us:us+len, 4) + x(us:us+len, 5) + x(us:us+len, 6))/6;

switch point
    case 1       
        y = (x(x1s:x1s+len, 1) + x(x1s:x1s+len, 2) + x(x1s:x1s+len, 3) + x(x1s:x1s+len, 4) + x(x1s:x1s+len, 5) + x(x1s:x1s+len, 6))/6;
    case 2
        y = (x(x2s:x2s+len, 1) + x(x2s:x2s+len, 2) + x(x2s:x2s+len, 3) + x(x2s:x2s+len, 4) + x(x2s:x2s+len, 5) + x(x2s:x2s+len, 6))/6;
    case 3
        y = (x(x3s:x3s+len, 1) + x(x3s:x3s+len, 2) + x(x3s:x3s+len, 3) + x(x3s:x3s+len, 4) + x(x3s:x3s+len, 5) + x(x3s:x3s+len, 6))/6;
end

% filtration
f = fir1(50, 0.92, 'low');
u = filter(f, 1, u);
% % u = u - mean(u);
y = filter(f, 1, y);
% % y = y - mean(y);

switch method
    case 1
        frf = modalfrf(u, y, Fs, 1000, 500);
        
    case 2
        order = 900;
        arData = iddata(y, [], 1/Fs);
        arModel = ar(arData, order);
        frf = modalfrf(arModel);
        
    case 3
        num = 270;
        den = 270;
        arxData = iddata(y, u, 1/Fs);
        arxModel = arx(arxData, [num den 0]);
        frf = modalfrf(arxModel);
        
    case 4
        frf = fft(conv(u, y));
        frf = frf(1:end/2);
        
    case 5
        frf = fft(u).*fft(y);
        frf = frf(1:end/2);
        
    case 6
        frf = pwelch(u, 2400, 0, Fs).*pwelch(y, 2400, 0, Fs);
        frf = frf(1:end/2);
end

a11 = u;
a12 = y;
a21 = -(20*log10(abs(frf)));
a22 = points(point).x;

