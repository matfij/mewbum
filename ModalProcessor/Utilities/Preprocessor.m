clc
clear
close all

addpath C:\Users\mateu\Desktop\MEWBUT\ModalAnalizer\Data
load('points')


for i = 1 : 9
    
    points(i).phase = angle(points(i).y);
    
    figure(1)
    subplot(3, 3, i)
    plot((points(i).x))
    hold on
    plot(0.1*max(points(i).x)*points(i).phase)
    
    figure(2)
    subplot(3, 3, i)
    plot((points(i).y))
    hold on
    plot(0.1*max(points(i).y)*points(i).phase)
end


cords = [1 4; 0 3; 1 3; 0 2; 1 2; 0 1; 1 1; 0 0; 1 0];
freqS = [295 315 571 616 646 860]; % samples
hold on
figure(2)
hold on


for i = 1 : 9 % each point
    
    for j = 1 : 6 % each frequency
        
        if (points(i).phase(freqS(j)) > 1)
            
            points(i).f(j) = max(abs( points(i).x(freqS(j)-5 : freqS(j)+5) ));
            
            subplot(3, 3, i)
            hold on
            plot(freqS(j), 0.12*max(points(i).x), 'ro')
            figure(1)
            subplot(3, 3, i)
            hold on
            plot(freqS(j), 0.12*max(points(i).x), 'ro')
            figure(2)
        else
            points(i).f(j) = -max(abs( points(i).x(freqS(j)-5 : freqS(j)+5) ));
            
            subplot(3, 3, i)
            hold on
            plot(freqS(j), 0.12*max(points(i).y), 'bo')
            figure(1)
            subplot(3, 3, i)
            hold on
            plot(freqS(j), 0.12*max(points(i).y), 'bo')
            figure(2)
        end
    end
    
    points(i).cx = cords(i, 1);
    points(i).cy = cords(i, 2);
    
end


















