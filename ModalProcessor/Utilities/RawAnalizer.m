function varargout = RawAnalizer(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @RawAnalizer_OpeningFcn, ...
                   'gui_OutputFcn',  @RawAnalizer_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end
if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
warning off
function RawAnalizer_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
guidata(hObject, handles);
function varargout = RawAnalizer_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function plotBtn_Callback(hObject, eventdata, handles)
    
    point = get(handles.pointMenu, 'Value');
    method = get(handles.methodMenu, 'Value');
    
    [a11, a12, a21, a22] = Analize(point, method);
    
    charts = [handles.axes11 handles.axes12 handles.axes21 handles.axes22];
    titles = {'input', 'output', 'obtained FRF', 'desired FRF'};
    for i = 1 : 4
        if (get(handles.overBox, 'Value') == 0)
            cla(charts(i))
        end
        axes(charts(i))
        hold on
        switch i
            case 1; plot(a11)
            case 2; plot(a12)
            case 3; plot(a21)
            case 4; plot(a22)
        end
        title(titles(i))
        pan on
%         zoom on
    end



    
    
    
    
    
    
    
