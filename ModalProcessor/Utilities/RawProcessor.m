clc
clear
close all

addpath ../Data
load('rawx1x2x3')
plot(x)
title('raw data')

Fs = 25000;

% excitation, p1, p2, p3 responses 
us = 3176;
x1s = 11879;
x2s = 20583;
x3s = 29287;
len = 5000;

data.u = (x(us:us+len, 1) + x(us:us+len, 2) + x(us:us+len, 3) + x(us:us+len, 4) + x(us:us+len, 5) + x(us:us+len, 6))/6;
data.y(1, :) = (x(x1s:x1s+len, 1) + x(x1s:x1s+len, 2) + x(x1s:x1s+len, 3) + x(x1s:x1s+len, 4) + x(x1s:x1s+len, 5) + x(x1s:x1s+len, 6))/6;
data.y(2, :) = (x(x2s:x2s+len, 1) + x(x2s:x2s+len, 2) + x(x2s:x2s+len, 3) + x(x2s:x2s+len, 4) + x(x2s:x2s+len, 5) + x(x2s:x2s+len, 6))/6;
data.y(3, :) = (x(x3s:x3s+len, 1) + x(x3s:x3s+len, 2) + x(x3s:x3s+len, 3) + x(x3s:x3s+len, 4) + x(x3s:x3s+len, 5) + x(x3s:x3s+len, 6))/6;

figure(1)
subplot(2, 2, 1)
plot(data.u)
title('excitation')
titles = {'excitation' 'p1 response' 'p2 response' 'p3 response'};

for i = 1 : 3 
    
    figure(1)
    subplot(2, 2, i+1)
    plot(data.y(i, :))
    title(titles(i+1))
    
    % base frf estimation
    [data.frf(i, :), data.f] = modalfrf(data.u, data.y(i, :), Fs, 1000, 500);
    figure(2)
    subplot(3, 3, i)
    plot(data.f, 20*log10(abs(data.frf(i, :))))
end

% parametric identification
arData1 = iddata(data.y(1, :)', [], 1/Fs);
arData2 = iddata(data.y(2, :)', [], 1/Fs);
arData3 = iddata(data.y(3, :)', [], 1/Fs);

arxData1 = iddata(data.y(1, :)', data.u, 1/Fs);
arxData2 = iddata(data.y(2, :)', data.u, 1/Fs);
arxData3 = iddata(data.y(3, :)', data.u, 1/Fs);

order = 60;
arMode1 = ar(arData1, order);
arMode2 = ar(arData1, order);
arMode3 = ar(arData1, order);

num = 90;
den = 90;
arxMode1 = arx(arxData1, [num den 0]);
arxMode2 = arx(arxData2, [num den 0]);
arxMode3 = arx(arxData3, [num den 0]);

[data.arFRF1, arf] = modalfrf(arMode1);
data.arFRF2 = modalfrf(arMode2);
data.arFRF3 = modalfrf(arMode3);

[data.arxFRF1, arxf] = modalfrf(arxMode1);
data.arxFRF2 = modalfrf(arxMode2);
data.arxFRF3 = modalfrf(arxMode3);

subplot(3, 3, 4)
plot(arf, 20*log10(abs(data.arFRF1)))
subplot(3, 3, 5)
plot(arf, 20*log10(abs(data.arFRF2)))
title('ar model estimation')
subplot(3, 3, 6)
plot(arf, 20*log10(abs(data.arFRF3)))

subplot(3, 3, 7)
plot(20*log10(abs(data.arxFRF1)))
subplot(3, 3, 8)
plot(20*log10(abs(data.arxFRF2)))
title('arx model estimation')
subplot(3, 3, 9)
plot(20*log10(abs(data.arxFRF3)))

