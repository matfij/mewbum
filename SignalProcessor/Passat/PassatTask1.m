function PassatTask1()

% clc
% clear
% close all

% task 1 - analysis of connection between harmonics and rpm
addpath Passat/PassatVibroData

names = {'1_2_1000.mat', '1_2_2000.mat', '1_2_3000.mat'};
    
titles = {'Alternator top - 1000RPM', 'Alternator top - 2000RPM', 'Alternator top - 3000RPM'};

tM = 10;
fs = 25000;
dt = 1/fs;
t = dt : dt : tM;
f = 0 : (1/tM) : fs;

win = hamming(2^13);
nov = 2^12;

for i = 1 : 3
    temp = load(string(names(i)));
    temp = temp.Vib1_noPM_noRAW_basic1s_Machine_1_Vibro_1_(:, 2);
    x(i).v = temp(1 : tM * fs);
    
    figure(1)
    subplot(3, 3, i)
    plot(t, x(i).v)
    title(string(titles(i)))
    xlabel('Time [s]')
    ylabel('Amplitude')
    ylim([-10 15])
   
    X(i).v = abs(fft(x(i).v));
    X(i).v = X(i).v/length(X(i).v);

    subplot(3, 3, i + 3)
    plot(f(1:floor(end/2)), X(i).v(1:floor(end/2)))
    title(strcat(string(titles(i)), ' - Fourier spectre'))
    xlabel('Frequency [Hz]')
    ylabel('Amplitude')
    ylim([0 0.4])
    
    [p, f] = pwelch(temp, win, nov, f, fs);
    p = 20*log10(p);
    
    subplot(3, 3, i + 6)
    plot(f(1:floor(end/2)), p(1:floor(end/2)))
    title(strcat(string(titles(i)), ' - Power spectre density [log]'))
    xlabel('Frequency [Hz]')
    ylabel('Amplitude [dB]')
    ylim([-160 0])
end













