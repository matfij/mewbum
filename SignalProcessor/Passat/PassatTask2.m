function PassatTask2()

% clc
% clear
% close all

% task 2 - analysis of damping on different measure points
addpath Passat/PassatVibroData

mesLow = {'2_1_ja這we.mat', '2_2_ja這we.mat', '2_3_ja這we.mat', ...
    	'2_4_ja這we.mat', '2_5_ja這we.mat', '2_6_ja這we.mat', ...
    	'2_7_ja這we.mat', '2_8_ja這we.mat', '2_9_ja這we.mat'};
    
mesHigh = {'2_1_max.mat', '2_2_max.mat', '2_3_max.mat', ...
        '2_4_max.mat', '2_5_max.mat', '2_6_max.mat', ...
        '2_7_max.mat', '2_8_max.mat', '2_9_max.mat'};
    
titles = {'1 - Blok silnika(gora)', '2 - Alternator', '3 - Przed poduszka silnika', ...
        '4 - Za poduszka silnika', '5 - Blok silnika(bok)', '6 - blok silnika(przod)', ...
        '7 - amortyzator(gora)', '8 - kierownica(przod)', '9 - kierownica(gora)'};
    
tM = 10;
fs = 25000;
dt = 1/fs;
t = dt : dt : tM;
f = 0 : (1/tM) : fs;

for i = 1 : 9
    temp = load(string(mesLow(i)));
    temp = temp.Vib1_noPM_noRAW_basic1s_Machine_1_Vibro_1_(:, 2);
    xLow(i).s = temp(1 : tM * fs);
    
    temp = load(string(mesHigh(i)));
    temp = temp.Vib1_noPM_noRAW_basic1s_Machine_1_Vibro_1_(:, 2);
    xHigh(i).s = temp(1 : tM * fs);
    
    xh(i, :) = downsample(xHigh(i).s, 10);
    xl(i, :) = downsample(xLow(i).s, 10);
end

figure(2)
subplot(1, 2, 2)
waterfall(xh)
title('Measured signals - 3000 rpm')
xlabel('time [n]')
ylabel('measuring point')
figure(2)
subplot(1, 2, 1)
waterfall(xl)
title('Measured signals - 1000 rpm')
xlabel('time [n]')
ylabel('measuring point')

for i = 1 : 9
    xLow(i).T = abs(fft(xLow(i).s));
    xLow(i).T = xLow(i).T/length(xLow(i).T);
    xHigh(i).T = abs(fft(xHigh(i).s));
    xHigh(i).T = xHigh(i).T/length(xHigh(i).T);
    
    txh(i, :) = downsample(xHigh(i).T(1:floor(end/2)), 10);
    txl(i, :) = downsample(xLow(i).T(1:floor(end/2)), 10);
end

figure(3)
subplot(1, 2, 2)
waterfall(txh)
title('Fourier spectre - 3000 rpm')
xlabel('frequency [Hz]')
ylabel('measuring point')
subplot(1, 2, 1)
waterfall(txl)
title('Fourier spectre - 1000 rpm')
xlabel('frequency [Hz]')
ylabel('measuring point')

for i = 1 : 9
    xLowPP(i) = peak2peak(xLow(i).s);
    xHighPP(i) = peak2peak(xHigh(i).s);

    xLowRMS(i) = rms(xLow(i).s);
    xHighRMS(i) = rms(xHigh(i).s);
end

figure(4)
subplot(2, 2, 1)
bar(xHighPP)
hold on
bar(xLowPP)
legend('3000 rpm', '1000 rpm')
title('Peak - Peak')
xlabel('Measuring point')
ylabel('Value')

subplot(2, 2, 2)
bar(xHighRMS)
hold on
bar(xLowRMS)
legend('3000 rpm', '1000 rpm')
title('Root mean square')
xlabel('Measuring point')
ylabel('Value')

subplot(2, 2, 3)
xLowDamp = 100*(1 - (xLowRMS/max(xLowRMS)));
bar(xLowDamp)
title('Damping - 1000RPM')
xlabel('Measuring point')
ylabel('Value [%]')

subplot(2, 2, 4)
xHighDamp = 100*(1 - (xHighRMS/max(xHighRMS)));
bar(xHighDamp)
title('Damping - 3000RPM')
xlabel('Measuring point')
ylabel('Value [%]')

