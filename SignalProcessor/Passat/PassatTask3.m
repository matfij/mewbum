function PassatTask3()

% clc
% clear
% close all

% task 3 - analysis in time-frequency domain
addpath Passat/PassatVibroData

n = 0.1 : 0.1 : 60;

u(10  : 50) = 0;
u(50  : 100) = 1000;
u(100  : 150) = 3000;
u(150  : 200) = 2000;
u(200  : 250) = 2000 - 400*n(1 : 51);
u(250 : 300) = 0;
u(300  : 350) = 1000*n(1 : 51);
u(350 : 400) = 5000;
u(400 : 450) = 1000;
u(450 : 550) = 2000;
u(550 : 600) = 2000 - 400*n(1 : 51);

length(n)
length(u)

figure(5)
subplot(2, 1, 1)
plot(n, u)
title('Idealized signal')
xlabel('Time [s]')
ylabel('RPM')

tM = 60;
fs = 25000;
dt = 1/fs;

x = load('3_1.mat');
x = x.Vib1_noPM_noRAW_basic1s_Machine_1_Vibro_1_(:, 2);
x = x(1 : tM * fs);

win = hamming(2^14);
nov = 2^13;
f = 0 : (1/tM) : fs;

subplot(2, 1, 2)
spectrogram(x, win, [], f(1:floor(end/2)), fs, 'yaxis')
title('Spectrogram')
zoom on


