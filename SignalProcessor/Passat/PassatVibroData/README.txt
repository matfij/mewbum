OPIS nazw

X_Y_A.mat

X - tytu� pomiaru:
1 - pomiar drga� 10s w sta�ych warunkach
2 - pomiar drga� w r�nych punktach w celu wyznaczenia t�umienia 10s
3 - pomiar przy wymuszeniu sygna�em niestacjonarnym 60s

Y - numer punktu pomiarowego:
1 - blok silnika pionowo I
2 - alternator pionowo V
3 - przed przedni� poduszk� pionowo I
4 - za przedni� poduszk� pionowo V
5 - blok silnika poprzecznie I
6 - blok silnika pod�u�nie V
7 - amortyzator pionowo I
8 - fotel poprzecznie I
9 - kierownica pionowo V

A - dodatkowe informacje:
ja�owe oznacza obroty ok 1000RPM
max oznacza obroty ok 3000RPM

kierunki pomiaru s� podane wzgl�dem osi samochodu:
pod�u�nie - od baga�nika do maski
poprzecznie - od drzwi do drzwi
pionowo - od podwozia do dachu

u�yte czujniki:
V - vis
I - imi