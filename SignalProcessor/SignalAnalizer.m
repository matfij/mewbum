function varargout = SignalAnalizer(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SignalAnalizer_OpeningFcn, ...
                   'gui_OutputFcn',  @SignalAnalizer_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end
if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
addpath UIControls
function SignalAnalizer_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
guidata(hObject, handles);
function varargout = SignalAnalizer_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% --- Executes on button press in btnLoadData.
function btnLoadData_Callback(hObject, eventdata, handles)
    try
        data = uiimport('-file');
        tM = str2double(get(handles.inputMeasurementLength, 'String'));
        fs = str2double(get(handles.inputSamplingFrequency, 'String'));
        handles.Signal = PlotData(handles, data, fs, tM); 
        guidata(hObject, handles)
    catch
        msgbox('Please load correct data.', 'Warning', 'warn')
    end
    

% --- Executes on button press in btnClear.
function btnClear_Callback(hObject, eventdata, handles)
    try
        ClearData(handles)
    catch
        msgbox('Function not found.', 'Error', 'error')
    end
    

% --- Executes on button press in analizeButton.
function analizeButton_Callback(hObject, eventdata, handles)
    try
        t = str2double(get(handles.inputMeasurementLength, 'String'));
        fs = str2double(get(handles.inputSamplingFrequency, 'String'));
        isLog = get(handles.checkLogScale, 'Value');
        type = get(get(handles.radioAnalizeType,'SelectedObject'),'Tag');
        win = str2double(get(handles.inputAnalizeWindow, 'String'));
        nov = str2double(get(handles.inputAnalizeOverlap, 'String'));
        
        AnalizeData(handles, handles.Signal, t, fs, type, isLog, win, nov)
    catch
        msgbox('Memory leak.', 'Error', 'error')
    end

    
% --- Executes on button press in btnRunTask.
function btnRunTask_Callback(hObject, eventdata, handles)
    try
        task = get(handles.menuTask, 'Value');
        RunNewTask(task);
    catch
        msgbox('Memory leak.', 'Error', 'error')
    end
    

% --- Executes on button press in btnCloseTask.
function btnCloseTask_Callback(hObject, eventdata, handles)
    try
        task = get(handles.menuTask, 'Value');
        CloseTask(task);
    catch
        msgbox('Task not found.', 'Error', 'error')
    end
