function AnalizeData(handles, signal, t, fs, method, isLog, win, nov)

axes(handles.axesBot)
cla

switch method
    case 'btnFourier'
        f = 0 : (1/t) : fs;
        
        s =  abs(fft(signal));
        if(isLog)
            s = 20*log10(s);
        end
        
        plot(f(1:floor(end/2)), s(1:floor(end/2)))
        xlabel('Frequency [Hz]')
        ylabel('Amplitude')
        
    case 'btnSpectrogram'
        window = hamming(win);
        f = 0 : (1/t) : fs;
        
        spectrogram(signal, window, nov, f(1:floor(end/2)), fs, 'yaxis')
        
    case 'btnPwelch'
        window = hamming(win);
        f = 0 : (1/t) : fs;
        
        [p, f] = pwelch(signal, window, nov, f, fs);
        
        if(isLog)
            p = 20*log10(p);
        end
        
        plot(f(1:floor(end/2)), p(1:floor(end/2)))
        xlabel('Frequency [Hz]')
        ylabel('Amplitude')
end

zoom on

sPP = peak2peak(signal);
sRMS = rms(signal);
sKurtosis = kurtosis(signal);

set(handles.labelPP, 'String', strcat(' ', num2str(sPP)));
set(handles.labelRMS, 'String', strcat(' ', num2str(sRMS)));
set(handles.labelKurtosis, 'String', strcat(' ', num2str(sKurtosis)));





