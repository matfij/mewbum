function ClearData(handles)

clc

axes(handles.axesBot)
cla reset
axes(handles.axesTop)
cla reset

set(handles.labelPP, 'String', '');
set(handles.labelRMS, 'String', '');
set(handles.labelKurtosis, 'String', '');

set(handles.analizeButton, 'Enable', 'off')