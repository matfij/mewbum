function CloseTask(task)

addpath Passat

switch task
    case 1
        f = figure(1);
        close(f)
        
    case 2
        f = [figure(2), figure(3), figure(4)];
        close(f)
        
    case 3
        f = figure(5)
        close(f)
end