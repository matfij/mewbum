function x = PlotData(handles, data, fs, tM)

x = data.Vib1_noPM_noRAW_basic1s_Machine_1_Vibro_1_(:, 2);
x = x - mean(x);

try
    dt = 1/fs;
    x = x(1 : tM * fs);
    t = dt : dt : tM;

    axes(handles.axesTop)
    plot(t, x)
    title('Raw data')
    xlabel('Time [s]')
    ylabel('Amplitude')
    zoom on
    
    set(handles.analizeButton, 'Enable', 'on')
catch
    msgbox('Incorrect signal properties', 'Error', 'warn')
end

