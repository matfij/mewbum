function AnalizeData(handles, signal, win, nov, method)

fs = 25000;
dt = 1/fs;
tm = length(signal)/fs;
t = dt : dt : tm;
f = 0 : (1/tm) : fs;

axes(handles.sPlot)
cla

switch method
    case 1
        s =  abs(fft(signal));
        
        plot(f(1:floor(end/2)), s(1:floor(end/2)))
        xlabel('Frequency [Hz]')
        ylabel('Amplitude')
        
    case 2
        window = hamming(win);
        [p, f] = pwelch(signal, window, nov, f, fs);
%         p = 20*log10(p);
        
        plot(f(1:floor(end/2)), p(1:floor(end/2)))
        xlabel('Frequency [Hz]')
        ylabel('Amplitude')
        
    case 3
        window = hamming(win);
        spectrogram(signal, window, nov, f(1:floor(end/2)), fs, 'yaxis')
end

end

