function ClearData(handles)

axes(handles.aPlot)
cla reset
axes(handles.afPlot)
cla reset
axes(handles.vPlot)
cla reset
axes(handles.xPlot)
cla reset
axes(handles.sPlot)
cla reset

clc
