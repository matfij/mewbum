function FilterData(handles, data, ff, fo, cf, im)

signal.a = data;

% measurement properties
fs = 25000;
dt = 1/fs;
tm = length(signal.a)/fs;
t = dt : dt : tm;

% filtration
[B, A] = butter(fo, ff, 'high');
signal.af = filtfilt(B, A, signal.a);

axes(handles.afPlot)
plot(t, signal.af)
title('Filtreated Acceleration')
xlabel('Time [s]')
ylabel('Amplitude [mm]')
grid on
zoom on

% integration + filtration
switch (im)
    case 1
        signal.v = cumsum(signal.af) / fs;
    case 2
        signal.v = cumtrapz(signal.af) / fs;
end
signal.vf = cf * filtfilt(B, A, signal.v);

axes(handles.vPlot)
plot(t, signal.vf)
title('Velocity')
xlabel('Time [s]')
ylabel('Amplitude [mm]')
grid on
zoom on

% integration + filtration
switch (im)
    case 1
        signal.x = cumsum(signal.vf) / fs;
    case 2
        signal.x = cumtrapz(signal.vf) / fs;
end
signal.xf = cf * filtfilt(B, A, signal.x);

axes(handles.xPlot)
plot(t, signal.xf)
title('Displacement')
xlabel('Time [s]')
ylabel('Amplitude [mm]')
grid on
zoom on




