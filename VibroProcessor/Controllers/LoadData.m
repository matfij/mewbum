function sig =  LoadData(handles, data)

% signal preparation
signal.a = data.Vib1_noPM_noRAW_basic1s_Machine_1_Vibro_1_(:, 2);
signal.a = 9810 * signal.a;

% measurement properties
fs = 25000;
dt = 1/fs;
tm = length(signal.a)/fs;
t = dt : dt : tm;

axes(handles.aPlot)
plot(t, signal.a)
title('Raw Acceleration')
xlabel('Time [s]')
ylabel('Amplitude [g]')
zoom on

sig = signal.a;