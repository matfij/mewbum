function varargout = Program(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Program_OpeningFcn, ...
                   'gui_OutputFcn',  @Program_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end
if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
addpath Controllers
addpath Tasks
function Program_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
guidata(hObject, handles);
function varargout = Program_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% --- Executes on button press in loadBtn.
function loadBtn_Callback(hObject, eventdata, handles)
    try
        data = uiimport('-file');
        handles.signal = LoadData(handles, data);
        guidata(hObject, handles)
    catch
        msgbox('Please load correct data.', 'Warning', 'warn')
    end
    

% --- Executes on button press in analizeBtn.
function analizeBtn_Callback(hObject, eventdata, handles)
%     try
        ff = str2double(get(handles.filterFrequency, 'String'));
        fo = str2double(get(handles.filterOrder, 'String'));
        cf = str2double(get(handles.compensationInput, 'String'));
        im = get(handles.integrationMenu, 'Value');
        FilterData(handles, handles.signal, ff, fo, cf, im);
        guidata(hObject, handles)
%     catch
%         msgbox('Please load correct data.', 'Warning', 'warn')
%     end


% --- Executes on button press in loadTaskBtn.
function loadTaskBtn_Callback(hObject, eventdata, handles)
    try
        task = get(handles.taskMenu, 'Value');
        LoadTask(task);
    catch
        msgbox('Task not found.', 'Error', 'error')
    end


% --- Executes on button press in clearTaskBtn.
function clearTaskBtn_Callback(hObject, eventdata, handles)
    try
        task = get(handles.taskMenu, 'Value');
        ClearTask(task);
    catch
        msgbox('Task not found.', 'Error', 'error')
    end
    

% --- Executes on button press in freqAnalizeBtn.
function freqAnalizeBtn_Callback(hObject, eventdata, handles)
    method = get(handles.methodMenu, 'Value');
    win = str2double(get(handles.windowInput, 'String'));
    nov = str2double(get(handles.overlapInput, 'String'));
    AnalizeData(handles, handles.signal, win, nov, method)


% --- Executes on button press in clearBtn.
function clearBtn_Callback(hObject, eventdata, handles)
    ClearData(handles)
