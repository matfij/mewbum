
scale = 9810;
compensation = 1.1;

addpath data
fan.a = load('Vibro [1]_20191029090826');
fan.a = scale * fan.a.Vib1_noPM_noRAW_basic1s_Machine_1_Vibro_1_(:, 2);
% fan.a = fan.a - mean(fan.a);

fs = 25000;
dt = 1/fs;
tm = length(fan.a)/fs;
t = dt : dt : tm;
f = 0 : 1/tm : fs;

% filtration
fHigh = 0.0001;
ord = 2;
[B, A] = butter(ord, fHigh, 'high');

fan.af = fan.a;% compensation * filtfilt(B, A, fan.a);

figure(4)
subplot(2, 2, 1)
plot(t, fan.a)
title('fan - raw acceleration')
xlabel('time [s]')
ylabel('amplitude [mm/s^2]')
subplot(2, 2, 2)
plot(t, fan.af)
title('fan - filtred acceleration')
xlabel('time [s]')
ylabel('amplitude [mm/s^2]')

% velocity, displacement calculation
fan.v = cumtrapz(fan.af) / fs;
fan.vf = compensation * filtfilt(B, A, fan.v);
fan.x = cumtrapz(fan.vf) / fs;

subplot(2, 2, 3)
plot(t, fan.vf)
title('fan - velocity')
xlabel('time [s]')
ylabel('amplitude [mm/s]')
subplot(2, 2, 4)
plot(t, fan.x)
title('fan - displacement (5mm)')
xlabel('time [s]')
ylabel('amplitude [mm]')


