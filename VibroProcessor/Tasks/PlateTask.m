figure(1)

scale = 9.81;

addpath data
accel = load('Vibro [1]_20191029090951.mat');
accel = accel.Vib1_noPM_noRAW_basic1s_Machine_1_Vibro_1_(:, 2);
accel = accel / scale;

fs = 25000;
dt = 1/fs;
tm = length(accel)/fs;
t = dt : dt : tm;

% signal segmentation
no = 30;
threshold = max(accel) / 100;
window = 5000;
indBuff = 1;

for i = 1 : no
   indStart = find(abs(accel(indBuff:end)) > threshold, 1, 'first') + indBuff;
   signals(i).a = accel(indStart : indStart + window);
   signals(i).t = indStart*dt : dt : (indStart + window)*dt;
   indBuff = indStart + window;
end

% time characteristics - accelerations
subplot(2, 2, 1)
plot(t, accel)
title('full signal - acceleration')
xlabel('time [s]')
ylabel('amplitude [m/s^2]')

subplot(2, 2, 2)
for i = 1 : no
    plot(signals(i).t, signals(i).a)
    hold on
end
title('partial signals - acceleration')
xlabel('time [n]')
ylabel('amplitude [m/s^2]')

win = hamming(4000);
nov = 0;

% frequency characteristics - power spectre density
fullPSD = pwelch(accel, win, nov);

sumPSD = zeros(1, length(fullPSD));
for i = 1 : no
   signals(i).psd = pwelch(signals(i).a, win, nov);
   sumPSD = sumPSD + signals(i).psd;
end
sumPSD = sumPSD / no;

subplot(2, 2, 3)
plot(fullPSD)
title('full signal - power spectre density')
xlabel('frequency [Hz]')
ylabel('amplitude')

subplot(2, 2, 4)
plot(sumPSD)
title('partial signals - power spectre density')
xlabel('frequency [Hz]')
ylabel('amplitude')







