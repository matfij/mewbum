
scale = 9810;
compensation = 1;

addpath data
sawLong.a = load('Vibro [1]_20191029090933');
sawShort.a = load('Vibro [1]_20191029090915');
sawLong.a = scale * sawLong.a.Vib1_noPM_noRAW_basic1s_Machine_1_Vibro_1_(:, 2);
sawShort.a = scale * sawShort.a.Vib1_noPM_noRAW_basic1s_Machine_1_Vibro_1_(:, 2);

fs = 25000;
dt = 1/fs;
sawLong.tm = length(sawLong.a)/fs;
sawShort.tm = length(sawShort.a)/fs;
sawLong.t = dt : dt : sawLong.tm;
sawShort.t = dt : dt : sawShort.tm;

% filtration
fHigh = 0.0001;
ord = 4;
[B, A] = butter(ord, fHigh, 'high');

sawLong.af = sawLong.a;% - mean(sawLong.a);
% sawLong.af = compensation * filtfilt(B, A, sawLong.a);
sawShort.af = compensation * filtfilt(B, A, sawShort.a);

figure(2)
subplot(2, 2, 1)
plot(sawLong.a)
title('long saw - raw acceleration')
xlabel('time [s]')
ylabel('amplitude [mm/s^2]')
subplot(2, 2, 2)
plot(sawLong.af)
title('long saw - filtred acceleration')
xlabel('time [s]')
ylabel('amplitude [mm/s^2]')
figure(3)
subplot(2, 2, 1)
plot(sawShort.a)
title('short saw - raw acceleration')
xlabel('time [s]')
ylabel('amplitude [mm/s^2]')
subplot(2, 2, 2)
plot(sawShort.af)
title('short saw - filtred acceleration')
xlabel('time [s]')
ylabel('amplitude [mm/s^2]')

% velocity, displacement calculation
sawLong.v = cumtrapz(sawLong.af) / fs;
sawLong.vf = compensation * filtfilt(B, A, sawLong.v);
sawLong.x = cumtrapz(sawLong.vf) / fs;

sawShort.v = cumtrapz(sawShort.af) / fs;
sawShort.vf = compensation * filtfilt(B, A, sawShort.v);
sawShort.x = cumtrapz(sawShort.vf) / fs;

figure(2)
subplot(2, 2, 3)
plot(sawLong.vf)
title('long saw - velocity')
xlabel('time [s]')
ylabel('amplitude [mm/s]')
subplot(2, 2, 4)
plot(sawLong.x)
title('long saw - displacement (8mm)')
xlabel('time [s]')
ylabel('amplitude [mm]')
figure(3)
subplot(2, 2, 3)
plot(sawShort.vf)
title('short saw - velocity')
xlabel('time [s]')
ylabel('amplitude [mm/s]')
subplot(2, 2, 4)
plot(sawShort.x)
title('short saw - displacement (3mm)')
xlabel('time [s]')
ylabel('amplitude [mm]')



