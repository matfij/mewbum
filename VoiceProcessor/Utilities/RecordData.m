%function RecordData()
clc
close all

fs = 44100;
dt = 1/fs;
tm = 4;
t = dt : dt : tm;
f = 0 : (1/tm) : fs;

recorder = audiorecorder(fs, 24, 1);
recordblocking(recorder, tm);

x = getaudiodata(recorder);

voice = find(x > 0.06 * max(x));
x = x(voice(1) : voice(end));

words(i).fish = x;
i = i + 1
