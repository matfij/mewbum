clc
close all

addpath ReferenceData
load('words.mat')
no = size(words, 2);

fs = 44100;
dt = 1/fs;
tm = 4;
t = dt : dt : tm;
f = 0 : (1/tm) : fs;

win = hamming(200);
nov = 100;

% preprocessing - spectrogram
for i = 1 : no
    
    words(i).CAT = abs(spectrogram(words(i).cat, win, nov));
    words(i).DOG = abs(spectrogram(words(i).dog, win, nov));
    words(i).FISH = abs(spectrogram(words(i).fish, win, nov));
    
    subplot(2, 2, 1)
    hold on
    spectrogram(words(i).cat, win, nov)
    title('CATS')
    subplot(2, 2, 2)
    hold on
    spectrogram(words(i).dog, win, nov)
    title('DOGS')
    subplot(2, 2, 3)
    hold on
    spectrogram(words(i).fish, win, nov)
    title('FISHES')
end

xfStart = 0.01;
xfEnd = 0.08;
xsStart = 0.6;
xsEnd = 0.7;

yfStart = 0.3;
yfEnd = 0.5;
ysStart = 0.65;
ysEnd = 0.8;

% features extraction
for i = 1 : no
    
    features(i).Ax = sum(sum(words(i).CAT(floor(xfStart*length(words(i).CAT(:, 1))) : floor(xfEnd*length(words(i).CAT(:, 1))), ...
        floor(xsStart*length(words(i).CAT(1, :))) : floor(xsEnd*length(words(i).CAT(1, :))))));
    
    features(i).Ix = sum(sum(words(i).DOG(floor(xfStart*length(words(i).DOG(:, 1))) : floor(xfEnd*length(words(i).DOG(:, 1))), ...
        floor(xsStart*length(words(i).DOG(1, :))) : floor(xsEnd*length(words(i).DOG(1, :))))));
    
    features(i).Ux = sum(sum(words(i).FISH(floor(xfStart*length(words(i).FISH(:, 1))) : floor(xfEnd*length(words(i).FISH(:, 1))), ...
        floor(xsStart*length(words(i).FISH(1, :))) : floor(xsEnd*length(words(i).FISH(1, :))))));
    
    features(i).Ay = 8*sum(sum(words(i).CAT(floor(yfStart*length(words(i).CAT(:, 1))) : floor(yfEnd*length(words(i).CAT(:, 1))), ...
        floor(ysStart*length(words(i).CAT(1, :))) : floor(ysEnd*length(words(i).CAT(1, :))))));
    
    features(i).Iy = 8*sum(sum(words(i).DOG(floor(yfStart*length(words(i).DOG(:, 1))) : floor(yfEnd*length(words(i).DOG(:, 1))), ...
        floor(ysStart*length(words(i).DOG(1, :))) : floor(ysEnd*length(words(i).DOG(1, :))))));
    
    features(i).Uy = 8*sum(sum(words(i).FISH(floor(yfStart*length(words(i).FISH(:, 1))) : floor(yfEnd*length(words(i).FISH(:, 1))), ...
        floor(ysStart*length(words(i).FISH(1, :))) : floor(ysEnd*length(words(i).FISH(1, :))))));
    
    subplot(2, 2, 4)
    hold on
    plot(features(i).Ax ,features(i).Ay, 'ro')
    hold on
    plot(features(i).Ix, features(i).Iy, 'go')
    hold on
    plot(features(i).Ux, features(i).Uy, 'bo') 
end

title('Word Classification')
xlabel('feature x')
ylabel('feature y')
legend('CATS', 'DOGS', 'FISHES')

% ann data preparation
trainingData = [features.Ax features.Ix features.Ux; features.Ay features.Iy features.Uy];
targetData = [zeros(length(features), 1); ones(length(features), 1); 2*ones(length(features), 1)]';

% clustering
catX = 0; catY = 0;
dogX = 0; dogY = 0;
fishX = 0; fishY = 0;

for i = 1 : no
    
    catX = catX + features(i).Ax;
    catY = catY + features(i).Ay;
    dogX = dogX + features(i).Ix;
    dogY = dogY + features(i).Iy;
    fishX = fishX + features(i).Ux;
    fishY = fishY + features(i).Uy;
end

R = 0.60;
catR = R * max(max([features.Ax]) - min([features.Ax]), max([features.Ay]) - min([features.Ay]));
dogR = R * max(max([features.Ix]) - min([features.Ix]), max([features.Iy]) - min([features.Iy]));
sifhR = R * max(max([features.Ux]) - min([features.Ux]), max([features.Uy]) - min([features.Uy]));

subplot(2, 2, 4)
hold on
viscircles([catX/no, catY/no], catR, 'Color', 'r')
hold on
viscircles([dogX/no, dogY/no], dogR, 'Color', 'g')
hold on
viscircles([fishX/no, fishY/no], sifhR, 'Color', 'b')






